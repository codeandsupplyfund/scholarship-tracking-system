# CSSF Scholarship Tracking System

Our homemade scholarship applicant and award tracking system. It does what it says on the tin!

## Valued quality attributes

The process of applying and managing applicants and awardees must prefer:

1. **Usability** - workflows must be executable with _minimum friction_.
2. **Accessibility** - applicants must be able to execute workflows on any typical computing device, e.g. laptop, phone, with a screen reader, etc.
3. **Autonomy** - workflows must not require constant attention from humans
4. **Security** - any collected data must be safely stored.
5. **Efficiency** - upfront collection of user data must be kept to a minimum
6. **Auditability** - the provenance of any data must be tracked and no data deleted
7. **Affordability** - the application must be affordable to operate and modify because our non-profit resources are limited!

## 2018- application process

1. Applicant applies online through [the form](https://docs.google.com/forms/d/e/1FAIpQLSc0LyUXuY3wsrRdN1V-2pMNulqQmvuehj20_m_a-M6wOlMjrw/viewform)
2. Google Forms Notifications sends CSSF Secretreasurer an email
3. CSSF Secretreasurer notifies #council-cssf-selector on C&S Slack
4. CSSF Secretreasurer marks application as "reviewed"
5. Discussion ensues in Slack thread
6. If the selection committee has questions, those questions are passed through CSSF Secretreasurer, who emails the candidate and marks the application as contacted
7. If the selection committee approves full or partial by simple majority vote, then the application is marked 
  * Voting is currently expressed in three forms:
    1. :new_moon: - No award: the application does not meet our expectations.
    1. :first_quarter_moon: or :last_quarter_moon: - Partial award: the application meets expectations but the award will be less than what was asked for.
    1. :full_moon: - Full award: the application meets expectations and the award can be what was asked for.
8. If the selection committee rejects, then the application is marked as rejected
9. CSSF Secretreasurer communicates the final decision to the applicant
10. If the application was approved, then the applicant is asked to fill out a contract and receives the most current disbursement instructions.
11. Applicant returns signed contract
12. "Applicant" becomes "awardee"
13. Awardee attends event and incurs expenses before, during, and after
14. Awardee submits expense report to CSSF Secretreasurer as instructed in disbursement instructions
15. CSSF Secretreasurer verifies expenses are allowed
16. CSSF Secretreasurer mails check via certified mail

There's a slightly different process after becoming an awardee if the awardee requests upfront coverage (e.g. we buy their airfare or reserve their hotel). When this happens, there's more of a manual process for accounting for how much the awardee has "spent" from their total award as each transaction that hits our account must be reconciled against an individual's award "account". We try to steer awardees away from this because of this accounting challenge.

### Data collected

These are questions applicants answer:

1. What is your name?
2. What is your email address?
3. What phone number can you be reached at?
4. What event do you want to attend?
5. Will you be speaking at the event?
6. When does the event begin?
7. Where is the event?
8. Where can we find out more information about the event?
9. From where will you be traveling at the time of the event?
10. What expenses do you request to be covered?
11. How much total aid are you requesting to attend this event?
12. Will you be able to attend if you are given only partial aid?
13. How does your background influence your interaction with the tech community, and why are you excited about attending this event?

Questions 6-8 are only necessary if the event is not in a list available for question 5.

### Application flags and metadata

* Timestamp
* Reviewed
* Contacted
* Awarded
* Notes

### Application State Changes

```mermaid
graph LR
  Submitted --> Reviewed
  Reviewed --> Voting
  Voting --> Approved
  Voting --> rejected{{Rejected}}
  Approved --> Awarded
  Awarded --> Accepted
  Awarded --> declined{{Declined}}
  Accepted --> Unclaimed
  Unclaimed --> claimed(Claimed)
  Unclaimed --> revoked{{Revoked}}
  Unclaimed --> cancelled{{Cancelled}}
```

There is another _accounting_ state, wherein Unclaimed is _split_ to Claimed and Returned.
We use this transition to reflect when an awardee spends less than they were awarded so that we can track overrequests.

## High level concepts

* `Question` - a piece of data collected to aid `Reviewer`s in deciding the outcome of an application
* `Application Form` - a form that contains `Question`s
* `Application Submission` - a set of answers to `Question`s posed by an `Application Form`
* `Submission Comment` - a free-form remark attached to an `Application Submission` by a `Reviewer`
* `Submission Status` - a flag indicating where in an evaluation workflow an `Application Submission` is
* `Contract Templates` - a template that generates a signable contract for an `Applicant`
* `Supporting Documents` - uploadable files associated with an `Application Submission` including expense reports and signed contracts
* `User` - logged-in human
* `Reviewer` - a `User` that may _comment_ on an `Application Submission`
* `Applicant` - a `User` associated as the _submitter_ of an `Application Submission`
* `Awardee` - an `Applicant` whose `Application Submission` is in a certain set of positive outcome `Submission Status` categories
